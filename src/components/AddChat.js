

  import React ,{ useState }from 'react'
  import firestore from '@react-native-firebase/firestore';
  import { SafeAreaView, StyleSheet, Text, View,TextInput,TouchableOpacity } from 'react-native'
  // import firebase from '@react-native-firebase/app';
  
  export default function AddChat({navigation}) {
    const [roomName, setRoomName] = useState('');

    function handleButtonPress() {
        if (roomName.length > 0) {
          firebase.auth().signInAnonymously()
              .then(()=> {
                return firebase.firestore().collection('THREADS').doc('BNrOV5zdqTyvVMgCDGE1').add({
                        // id:'fadsa'
                        name:roomName,
                        
                  });
                  
                  
              }).catch((err)=>{
                navigation.navigate('homepage');
                
                alert(err);
              })
            // firestore()
            // .collection('THREADS')
            // .doc('Room1')
            // .set({
            //   name: roomName,
              
            // })
            // .then(() => {
            //   navigation.navigate('homepage');
            //   // navigation.goBack();
            // });
        }
      }
      return (
        <SafeAreaView>

        <View style={{marginTop:30}}>
                <View style={{marginLeft:35}}>
                    <Text style={{fontSize:18,fontWeight:'bold'}}>Add Title of Chatting </Text>

                    <TextInput placeholder="  Title of Chatting " style={styles.input}
                            value={roomName}
                            onChangeText={text => setRoomName(text)}
                           
                      
                       />


                    <TouchableOpacity style={styles.button}  onPress={() => handleButtonPress()} disabled={roomName.length === 0}> 
                                  <Text style={{textAlign:'center',fontSize:20,fontWeight:'bold'}}>Creates</Text>
                    </TouchableOpacity>


                </View>
         </View>

</SafeAreaView>
      )
  }
  
  const styles = StyleSheet.create({

    container: {
                flex: 1,
                alignItems: 'center',
                justifyContent: 'center',
               
                marginTop:200
              },
              input: {
                width: 330,
                height: 44,
                padding: 10,
                borderWidth: 1,
                borderColor: 'black',
               marginTop:30,
                
                backgroundColor:"white",
                
              },
              button:{
                  width:330,
                  height:44,
                  padding:10,
                  borderWidth:1,
                  backgroundColor:'#f0564a',
                  marginTop:30,
                  borderRadius:30
              }
  
  });





// // import React from 'react'
// // import { StyleSheet, Text, View } from 'react-native'
// import React, { useState, useEffect, useCallback } from 'react'
// import { GiftedChat } from 'react-native-gifted-chat'
// import AsyncStorage from '@react-native-community/async-storage'
// import { StyleSheet, TextInput, View,YellowBox, Button } from 'react-native'


// const firebaseConfig = {
//     //Your firebase config here
// }

// if (firebase.apps.length === 0) {
//     firebase.initializeApp(firebaseConfig)
// }

// YellowBox.ignoreWarnings(['Setting a timer for a long period of time'])

// const db = firebase.firestore()
// const chatsRef = db.collection('chats')
// const AddChat = () => {
//     const [user, setUser] = useState(null)
//     const [name, setName] = useState('')
//     const [messages, setMessages] = useState([])

//     useEffect(() => {
//         readUser()
//         const unsubscribe = chatsRef.onSnapshot((querySnapshot) => {
//             const messagesFirestore = querySnapshot
//                 .docChanges()
//                 .filter(({ type }) => type === 'added')
//                 .map(({ doc }) => {
//                     const message = doc.data()
//                     //createdAt is firebase.firestore.Timestamp instance
//                     //https://firebase.google.com/docs/reference/js/firebase.firestore.Timestamp
//                     return { ...message, createdAt: message.createdAt.toDate() }
//                 })
//                 .sort((a, b) => b.createdAt.getTime() - a.createdAt.getTime())
//             appendMessages(messagesFirestore)
//         })
//         return () => unsubscribe()
//     }, [])

//     const appendMessages = useCallback(
//         (messages) => {
//             setMessages((previousMessages) => GiftedChat.append(previousMessages, messages))
//         },
//         [messages]
//     )

//     async function readUser() {
//         const user = await AsyncStorage.getItem('user')
//         if (user) {
//             setUser(JSON.parse(user))
//         }
//     }
//     async function handlePress() {
//         const _id = Math.random().toString(36).substring(7)
//         const user = { _id, name }
//         await AsyncStorage.setItem('user', JSON.stringify(user))
//         setUser(user)
//     }
//     async function handleSend(messages) {
//         const writes = messages.map((m) => chatsRef.add(m))
//         await Promise.all(writes)
//     }
//     if(!user){

//         return (
//             <View style={styles.container}>
//                     <TextInput style={styles.input} placeholder="Enter your name" value={name} onChangeText={setName} />
//                     <Button onPress={handlePress} title="Enter the chat" />
//                 </View>
//         )

//     }
//     return <GiftedChat messages={messages} user={user} onSend={handleSend} />
    
// }

// export default AddChat

// const styles = StyleSheet.create({
//     container: {
//         flex: 1,
//         backgroundColor: '#fff',
//         alignItems: 'center',
//         justifyContent: 'center',
//         padding: 30,
//     },
//     input: {
//         height: 50,
//         width: '100%',
//         borderWidth: 1,
//         padding: 15,
//         marginBottom: 20,
//         borderColor: 'gray',
//     },
// })
