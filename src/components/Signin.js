

import React from 'react'

import { SafeAreaView, Text, TextInput, View ,StyleSheet, Button, TouchableOpacity} from 'react-native'
import {SignInUser} from '../apiservers/apiServices'

const Signin = ({navigation}) => {

  const [state, setState] = React.useState({
    emailAddress: '',
    password: '',
  });
  const [user, setUser] = React.useState();

  const signIn = () => {
    SignInUser(state.emailAddress, state.password)
      .then((data) => {
        // alert(data);
        navigation.navigate('homepage')
      })
      .catch((error) => {
        alert(error);
      });
     
  };

  const onAuthStateChanged = (user) => {
    setUser(user);
  };

   const useEffect=(() => {
    const subscriber = Auth().onAuthStateChanged(onAuthStateChanged);
    return subscriber;
  }, []);
  return (
   
    <SafeAreaView>
                 <View >
                        <View style={{justifyContent:'center',alignItems:'center',marginTop:150}}>
                           <Text style={{fontSize:35,fontWeight:'bold',color:'blue'}}>Hey Login Here</Text>
                         </View>
                       
                        <View style={{justifyContent:'center',alignItems:'center',marginTop:55}}>
                                 <TextInput placeholder="email..." style={styles.input}
                                     value={state.emailAddress}
                                     onChangeText={(text) => setState({...state, emailAddress: text})}
                                 />
                         </View>
                         <View style={{justifyContent:'center',alignItems:'center',marginTop:5}}>
                               <TextInput placeholder="password..." style={styles.input}
                                 value={state.password}
                                 onChangeText={(text) => setState({...state, password: text})}
                               />
                         </View>
                         <View style={{justifyContent:'center',alignItems:'center',marginTop:5}}>
                               <TouchableOpacity style={styles.button} onPress={signIn}> 
                                          <Text style={{textAlign:'center',fontSize:20,fontWeight:'bold'}}>Login</Text>
                               </TouchableOpacity>
                              
    
                         </View>
                         <Button title="signup "  onPress={()=> navigation.navigate('signup')}></Button>
                     
                                                      
    
                        
           </View>
    </SafeAreaView>
  )
}

export default Signin

const styles = StyleSheet.create({

  container: {
              flex: 1,
              alignItems: 'center',
              justifyContent: 'center',
             
              marginTop:200
            },
            input: {
              width: 330,
              height: 44,
              padding: 10,
              borderWidth: 1,
              borderColor: 'black',
              marginBottom: 10,
              borderRadius:30,
              backgroundColor:"white",
              
            },
            button:{
                width:330,
                height:44,
                padding:10,
                borderWidth:1,
                backgroundColor:'#97b9b5',
                borderRadius:30
            }

});

