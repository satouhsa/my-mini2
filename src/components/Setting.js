

import React from 'react'
import { StyleSheet, Text, View,Image } from 'react-native'
import { Divider } from 'react-native-elements'
import { SafeAreaView } from 'react-native-safe-area-context'
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faBell,FaTwitter} from '@fortawesome/free-solid-svg-icons';
import { TouchableOpacity } from 'react-native-gesture-handler';
import {Auth} from "../apiservers/setUp"
import {SignOutUser} from '../apiservers/apiServices'

const 
const Setting = ({navigation}) => {

    const [user, setUser] = React.useState();

    const signOut = () => {
        SignOutUser()
          .then((data) => {
              navigation.navigate('login')
            // alert(data);
          })
          .catch((error) => {
            alert(error);
          });
      };
    
      const onAuthStateChanged = (user) => {
        setUser(user);
      };
      const useEffect=(() => {
        const subscriber = Auth().onAuthStateChanged(onAuthStateChanged);
        return subscriber;
      }, []);
    return (
       
        <SafeAreaView>

                        <View>
                                    <View>
                                        <Image
                                            source={{uri:'https://www.shareicon.net/data/2015/11/30/680132_users_512x512.png'}}
                                            style={{width:80,height:80,marginLeft:180}}
                                        />
                                       <Text style={{textAlign:'center',marginTop:20}}>{}</Text>
                                       <Text style={{marginLeft:25,marginTop:10}} >fashion store for women and men . </Text>
                                        <Divider style={{width:120,marginLeft:180,marginTop:20}}></Divider>
                                    </View>

                                    <View>
                                        <Text style={{marginLeft:25,marginTop:20}}> 
                                             Fashion trends influenced by several factors, including cinema, celebrities, climate, creative explorations,
                                        </Text>
                                        <Divider style={{width:120,marginLeft:180,marginTop:20}}></Divider>
                                    </View>

                                    <View>
                                            <Text style={{marginLeft:25,marginTop:20}}>Find me on Social here</Text>
                                            <View style={{flex: 1, flexDirection: 'row',marginLeft:200}}>
                                               <View>
                                                      <FontAwesomeIcon icon={faBell} size={30} color={"black"}style={{marginTop:15,marginLeft:14,borderRadius:10}} />
                                               </View>
                                                <View>
                                                <FontAwesomeIcon icon={faBell} size={30} color={"black"}style={{marginTop:15,marginLeft:14,borderRadius:10}} />
                                                </View>
                                                <View>
                                                <FontAwesomeIcon icon={faBell} size={30} color={"black"}style={{marginTop:15,marginLeft:14,borderRadius:10}} />
                                                </View>
                                            </View>
                                            <Divider style={{width:120,marginLeft:180,marginTop:80}}></Divider>
                                    </View>


                                    <View style={{marginTop:30,marginLeft:30}}>
                                       
                                            <TouchableOpacity onPress={signOut}>
                                                  <Text style={{fontSize:15,fontWeight:'bold',color:'blue'}}>Logout</Text>
                                             </TouchableOpacity>

                                       
                                        
                                    </View>
                        </View>
            </SafeAreaView>
    )
}

export default Setting



