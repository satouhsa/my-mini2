


import React , { useState, useEffect }from 'react'
import { View, Text,StyleSheet } from 'react-native'
import { Container, Header, Content, List} from 'native-base';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faLock, faSearch, faUser, faUsers } from '@fortawesome/free-solid-svg-icons';
import { FlatList, TouchableOpacity } from 'react-native-gesture-handler';
import firestore from '@react-native-firebase/firestore';
import { Divider } from 'react-native-elements';

export default function Homepage({navigation}) {

    const [threads, setThreads] = useState([]);
  const [loading, setLoading] = useState(true);

 const useEffect=(() => {
    const unsubscribe = firestore()
      .collection('THREADS')
      .onSnapshot(querySnapshot => {
        const threads = querySnapshot.docs.map(documentSnapshot => {
          return {
            id: documentSnapshot.id,
            // give defaults
            name: '',
            ...documentSnapshot.data()
          };
        });

        setThreads(threads);

        if (loading) {
          setLoading(false);
        }
      });

    /**
     * unsubscribe listener
     */
    return () => unsubscribe();
  }, []);

//   if (loading) {
//     return <Loading />;
//   }



    return (
        <Container>
                        <Header style={{backgroundColor:'#c5e1e4'}}>

                        <View style={{flex: 1, flexDirection: 'row'}}>
                            <View style={{flex:1}}>
                                <TouchableOpacity onPress={()=>navigation.navigate('My Profile')}>
                                    <FontAwesomeIcon icon={faUser} size={30} color={"black"}style={{marginTop:15,marginLeft:14,borderRadius:10}} />
                                </TouchableOpacity>
                                 
                            </View>
                            <View style={{flex:3,marginTop:18}}>

                                    <Text style={{fontSize:27,fontWeight:'bold'}}>Chat</Text>

                            </View>
                            <View>
                                <TouchableOpacity onPress={()=> navigation.navigate('Add Chat')}>
                                             <FontAwesomeIcon icon={faSearch} size={30} color={"black"} style={{marginTop:15,marginRight:25}}/>
                                </TouchableOpacity>
                               
                            </View>
                            <View>
                                <TouchableOpacity onPress={()=>this.props.navigation.navigate('Add Group')}>
                                             <FontAwesomeIcon icon={faUsers} size={30} color={"black"} style={{marginTop:15}}/>
                                </TouchableOpacity>
                                  
                            </View>
                         </View>

                        </Header>
                        <Content>

                        <View style={styles.container}>
                                <FlatList
                                    data={threads}
                                    keyExtractor={item => item.id}
                                    ItemSeparatorComponent={() => <Divider />}
                                    renderItem={({ item }) => (
                                   
                                        <List.Item
                                        title={item.name}
                                        description="Item description"
                                        titleNumberOfLines={1}
                                       
    
                                        descriptionNumberOfLines={1}
                                        />
                                    
                                    )}
                                />
                            </View>
                        
                        </Content>
                </Container>
    )
}

const styles = StyleSheet.create({
    container: {
      backgroundColor: '#f5f5f5',
      flex: 1
    },
    listTitle: {
      fontSize: 22
    },
    listDescription: {
      fontSize: 16
    }
  });