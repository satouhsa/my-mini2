import React from 'react'
import { View, Text } from 'react-native'
import firebase from '@react-native-firebase/app';
import Auth from '@react-native-firebase/auth';
import App from '../../App';

const firebaseConfig = {
    apiKey: 'AIzaSyDjSJpW2oWc6pLCP9bbjSYiJtGfS4tcsAc',
    authDomain: 'fir-demo-e2751.firebaseapp.com',
    databaseURL: 'https://fir-demo-e2751.firebaseio.com',
    projectId: 'fir-demo-e2751',
    storageBucket: 'fir-demo-e2751.appspot.com',
    messagingSenderId: '81014207550',
    appId: '1:81014207550:web:0a4f29e52f2e01ee38cd6f',
  };
  
  if (!firebase.apps.length) {
    firebase.initializeApp(firebaseConfig);
  }

  export {
    firebase,
    Auth,
    
  };

const setUp = () => {
    return 
        (
            <App/>
        )
    
}

export default setUp
