import React, { Component } from 'react'
import { Text, View } from 'react-native'
import Signin from './src/components/Signin'
import Signup from './src/components/Signup'
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Homepage from './src/components/Homepage';
import Setting from './src/components/Setting';
import AddChat from './src/components/AddChat';
import AddGroup from './src/components/AddGroup';

const Stack = createStackNavigator();

export class App extends Component {
  render() {
    return (
      <NavigationContainer>
        <Stack.Navigator>
          <Stack.Screen name="login" component={Signin}
            options={{headerShown: false}}
          />
          <Stack.Screen name="signup" component={Signup}
            options={{headerShown: false}}
          />
          <Stack.Screen name="homepage" component={Homepage}
           options={{headerShown: false}}
          />
          <Stack.Screen name="My Profile" component={Setting}
          />
           <Stack.Screen name="Add Chat" component={AddChat}
          />
          <Stack.Screen name="Add Group" component={AddGroup}
          />
        </Stack.Navigator>
      </NavigationContainer>
    )
  }
}

export default App
